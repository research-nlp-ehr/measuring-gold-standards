import connect_umls_pkg as um
import json

# this is a way to quickly add synonyms
f = open("medmentions_synonyms.txt","a")
# you want to put your own UMLS api key here, or import it from an env.
API_KEY = ""
cui = um.CUI(API_KEY, "C0854135", "blegh")
synonyms = cui.synonyms()
f.write(cui.cui +"|!|"+"|".join(synonyms)+"\n")
f.close()

def makeSynsDict(syns):
    ret = {}
    for syn in syns:
        if len(syn.split("|")) < 2:
            continue
        code = syn.split("|")[0]
        synonyms =  syn.split("|")[1].split(",")
        ret[code] = synonyms
    return ret


def writeSynsDict(codes):
    codes = list(set(codes))
    f = open("medmentions_synonyms.txt", "a")
    print(len(codes), "...to go!")
    for i in range(len(codes)):
        code = codes[i]
        print("code", code)
        #REMEMBER TO COMMENT OUT CUI IF I EVER PUBLISH THIS
        cui =um.CUI(API_KEY, code, "boo")
        synonyms = cui.synonyms()
        f.write(code+"|"+",".join(synonyms)+"\n")
        print(len(codes)-i-1, "...to go!")
    f.close()

def loadMedMentions():
    f = open("medmentions.json")
    medmentions = f.read().split("\n")[:50]
    #UMLS = um.UMLS(")
    #f2 = open("medmentions_error.txt","w")
    all_codes = []
    for mention in medmentions:
        if mention == "": continue
        mentiondict = json.loads(mention)
        text = mentiondict["text"]
        codes = mentiondict["mentions"]
        for code in codes:

            subtext = text[code["start_offset"]:code["end_offset"]]
            cui = code["link_id"]
            all_codes.append(cui)
    syns = makeSynsDict(open("medmentions_synonyms.txt", "r").read().split("\n"))
    all_codes = set(all_codes).difference(set(syns.keys()))
    writeSynsDict(all_codes)

def parseMedMentions():
    f = open("medmentions.json")
    f2 = open("medmentions.txt", "w")
    medmentions = f.read().split("\n")[:50]
    #UMLS = um.UMLS(")
    #f2 = open("medmentions_error.txt","w")
    all_codes = []
    for mention in medmentions:
        if mention == "": continue
        mentiondict = json.loads(mention)
        text = mentiondict["text"]
        codes = mentiondict["mentions"]
        for code in codes:

            subtext = text[code["start_offset"]:code["end_offset"]]
            cui = code["link_id"]
            f2.write(subtext+"|"+cui+"|1.0\n")
    f2.close()

#parseMedMentions()
#loadMedMentions()
