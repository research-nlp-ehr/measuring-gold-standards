from Levenshtein import ratio
from nltk.tokenize import TweetTokenizer
from nltk.translate import bleu
import matplotlib.pyplot as plt
import numpy as np
# returns the highest rated back translation
def backTranslate(true,synonyms, score = ratio):
    max = 0.0
    max_string = ""
    for synonym in synonyms:
        val =score(true, synonym)
        if val > max:
            max = val
            max_string = synonym
    return max_string, max

def scoreBLEU1Gram(true, synonym):
    tnzr = TweetTokenizer()
    trueTokens = tnzr.tokenize(true)
    synonymTokens = tnzr.tokenize(synonym)
    return bleu([trueTokens], synonymTokens, [1.0])

def scoreBLEU2Gram(true, synonym):
    tnzr = TweetTokenizer()
    trueTokens = tnzr.tokenize(true)
    synonymTokens = tnzr.tokenize(synonym)
    return bleu([trueTokens], synonymTokens, [0.5, 0.5])

def makeSynsDict(syns):
    ret = {}
    for syn in syns:
        if len(syn.split("|!|")) < 2:
            continue
        code = syn.split("|!|")[0]
        synonyms =  syn.split("|!|")[1].split("|")
        ret[code] = synonyms
    return ret

def calculateScores(k, optionFile, synonymFile):
    options = open(optionFile).read().split("\n")
    synonyms = makeSynsDict(open(synonymFile).read().split("\n"))
    manualScore = []
    bleuScore1Gram = []
    bleuScore2Gram = []
    levenshteinScore = []
    for j in range(len(options)):
        option = options[j]
        if len(option.split("|")) != 3: continue
        true = option.split("|")[0]
        codes = option.split("|")[1].split(",")
        manual=option.split("|")[2].split(",")
        rangeK = k
        if k > len(codes):
            rangeK=len(codes)
        for i in range(rangeK):
            if manual[i] != "":
                manualScore.append(float(manual[i]))
                bleuScore1Gram.append(backTranslate(true, synonyms[codes[i]], scoreBLEU1Gram)[1])
                bleuScore2Gram.append(backTranslate(true, synonyms[codes[i]], scoreBLEU2Gram)[1])
                levenshteinScore.append(backTranslate(true, synonyms[codes[i]])[1])
    return  [manualScore, bleuScore1Gram, bleuScore2Gram, levenshteinScore]

def printAverage(k, optionFile="umls_coded_set.txt", synonymFile="umls_synonyms.txt"):
    data = calculateScores(k, optionFile, synonymFile)
    errors = calculateError(data)
    print("k:", k, "optionFile:", optionFile)
    labels=["Manual Score", "Cumulative BLEU Score 1-Gram", "Cumulative BLEU Score 2-Gram", "Levenshtein Score"]
    for i in range(4):
        print(labels[i])
        print("Score Average:",np.average(data[i]))
        if i > 0:
            print("Error Average:",np.average(errors[i-1]))
    print("...")
    print("")


def calculateError(data):
    levenshteinError = absolute_error(data[0], data[3])
    bleuScore1GramError = absolute_error(data[0], data[1])
    bleuScore2GramError = absolute_error(data[0], data[2])
    data = [bleuScore1GramError, bleuScore2GramError,levenshteinError]
    return data

def makeBoxPlotCode(k, title, error=False, optionFile="umls_coded_set.txt", synonymFile="umls_synonyms.txt"):
    labels = ["Manual Score", "Cumulative BLEU Score 1-Gram", "Cumulative BLEU Score 2-Gram", "Levenshtein Score"]
    data = calculateScores(k, optionFile, synonymFile)
    xlabel="Back-Translation Score"
    if error:
        xlabel="Back-Translation Absolute Error"
        data =calculateError(data)
        labels = ["Cumulative BLEU Score 1-Gram", "Cumulative BLEU Score 2-Gram", "Levenshtein Score"]


    plt.hist(data, bins=10, label=labels)
    plt.xlabel(xlabel)
    plt.ylabel("Frequency")
    plt.legend()
    plt.title(title)
    plt.show()


def absolute_error(y, y_pred):
    return np.abs(np.array(y) - np.array(y_pred))



def makeBoxPlots():
    makeBoxPlotCode(1, "Back-Translation Scores for Different Measurement Methods, Best Code per Annotation (Yates and Goharian)")
    makeBoxPlotCode(10, "Back-Translation Scores for Different Measurement Methods, All Possible Codes per Annotation (Yates and Goharian)")
    makeBoxPlotCode(1, "Back-Translation Absolute Error for Different Measurement Methods, Best Code per Annotation (Yates and Goharian)", True)
    makeBoxPlotCode(10, "Back-Translation Absolute Error for Different Measurement Methods, All Possible Codes per Annotation (Yates and Goharian)",True)
    makeBoxPlotCode(1, "Back-Translation Scores for Different Measurement Methods, Best Code per Annotation (MedMentions)", False, "medmentions.txt", "medmentions_synonyms.txt")
    makeBoxPlotCode(1, "Back-Translation Absolute Error for Different Measurement Methods, Best Code per Annotation (MedMentions)", True, "medmentions.txt", "medmentions_synonyms.txt")

def printAverages():
    printAverage(1)
    printAverage(10)
    printAverage(1,"medmentions.txt", "medmentions_synonyms.txt")

printAverages()
#makeBoxPlots()
